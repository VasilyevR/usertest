-- 1. Show Stores, that have products with Christmas, Winter Tags
SELECT DISTINCT Store.name
FROM Tag
JOIN TagConnect on Tag.id = TagConnect.tag_id and Tag.tag_name in ('Christmas', 'Winter')
JOIN Product on TagConnect.product_id = Product.id
JOIN Store on Product.store_id = Store.id
ORDER BY Store.name;

-- 2. Show Users, that never bought Product from Store with id == 5
SELECT `User`.name, count(`Store`.id) as cnt
FROM `User`
LEFT JOIN `Order` on `User`.id = `Order`.customer_id
LEFT JOIN `OrderItem` on `Order`.id = `OrderItem`.order_id
LEFT JOIN `Product` on `OrderItem`.product_id = `Product`.id
LEFT JOIN `Store` on `Product`.store_id = `Store`.id and `Store`.id = 5
GROUP BY `User`.id
HAVING cnt = 0
ORDER BY `User`.id;

-- 3. Show Users, that had spent more than $1000
SELECT `User`.name, sum(`Product`.price) as spent
FROM `OrderItem`
JOIN `Product` on `OrderItem`.product_id = `Product`.id
JOIN `Order` on `OrderItem`.order_id = `Order`.id
JOIN `User` on `Order`.customer_id = `User`.id
GROUP BY `User`.id
HAVING spent > 1000

-- 4. Show Stores, that have not any Sells
SELECT `Store`.name, count(`Product`.id) as cnt
FROM `Store`
LEFT JOIN `Product` on `Store`.id = `Product`.store_id
LEFT JOIN `OrderItem` on `Product`.id = `OrderItem`.product_id
GROUP BY `Store`.id
HAVING cnt = 0

-- 5. Show Mostly sold Tags
SELECT `Tag`.tag_name, count(`Product`.id) as cnt
FROM `Tag`
LEFT JOIN `TagConnect` on `Tag`.id = `TagConnect`.tag_id
LEFT JOIN `Product` on `TagConnect`.product_id = `Product`.id
LEFT JOIN `OrderItem` on `Product`.id = `OrderItem`.product_id
GROUP BY `Tag`.id
ORDER BY cnt desc

-- 6. Show Monthly Store Earnings Statistics
SELECT MONTHNAME(`Order`.order_date) as month, `Store`.name, sum(`Product`.price) as earned
FROM `Store`
JOIN `Product` on `Store`.id = `Product`.store_id
JOIN `OrderItem` on `Product`.id = `OrderItem`.product_id
JOIN `Order` on `OrderItem`.order_id = `Order`.id
GROUP BY `Store`.id, month
ORDER BY MONTH(`Order`.order_date), `Store`.id