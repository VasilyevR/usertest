<?php
declare(strict_types=1);

namespace UserTest\User;

class Seller extends AbstractUser implements IUser
{
    /**
     * @var string
     */
    private $earnings_balance;

    /**
     * @var string
     */
    private $product_count;

    /**
     * @param int $id
     * @param string $name
     * @param string $earnings_balance
     * @param string $product_count
     */
    public function __construct(int $id, string $name, string $earnings_balance, string $product_count)
    {
        parent::__construct($id, $name);
        $this->earnings_balance = $earnings_balance;
        $this->product_count = $product_count;
    }

    /**
     * {@inheritDoc}
     */
    public function getInfoData(): array
    {
        return [
            'user-type' => 'Seller',
            'id' => $this->id,
            'name' => $this->name,
            'earnings-balance' => $this->earnings_balance,
            'product-count' => $this->product_count,
        ];
    }
}
