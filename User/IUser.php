<?php
declare(strict_types=1);

namespace UserTest\User;

interface IUser
{
    /**
     * @return array
     */
    public function getInfoData(): array;
}