<?php
declare(strict_types=1);

namespace UserTest\User;

class Customer extends AbstractUser implements IUser
{
    /**
     * @var string
     */
    private $balance;

    /**
     * @var string
     */
    private $purchase_count;

    /**
     * @param int $id
     * @param string $name
     * @param string $balance
     * @param string $purchase_count
     */
    public function __construct(int $id, string $name, string $balance, string $purchase_count)
    {
        parent::__construct($id, $name);
        $this->balance = $balance;
        $this->purchase_count = $purchase_count;
    }

    /**
     * {@inheritDoc}
     */
    public function getInfoData(): array
    {
        return [
            'user-type' => 'Customer',
            'id' => $this->id,
            'name' => $this->name,
            'balance' => $this->balance,
            'purchase-count' => $this->purchase_count,
        ];
    }
}
