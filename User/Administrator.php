<?php
declare(strict_types=1);

namespace UserTest\User;

class Administrator extends AbstractUser implements IUser
{
    /**
     * @var string
     */
    private $permissions;

    /**
     * @param int $id
     * @param string $name
     * @param string $permissions
     */
    public function __construct(int $id, string $name, string $permissions)
    {
        parent::__construct($id, $name);
        $this->permissions = $permissions;
    }

    /**
     * {@inheritDoc}
     */
    public function getInfoData(): array
    {
        return [
            'user-type' => 'Administrator',
            'id' => $this->id,
            'name' => "'$this->name'",
            'permissions' => $this->permissions,
        ];
    }
}
