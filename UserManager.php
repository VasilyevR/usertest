<?php
declare(strict_types=1);

namespace UserTest;

use UserTest\User\IUser;

class UserManager
{
    /**
     * @param IUser $user
     * @return string
     */
    public function getUserInfo(IUser $user): string {
        $output = '';
        $data = $user->getInfoData();

        foreach ($data as $field => $value) {
            $output .= sprintf('// %s: %s%s', $field, $value, PHP_EOL);
        }
        $output .= PHP_EOL;

        return $output;
    }
}
