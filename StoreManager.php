<?php
class StoreManager
{
    /**
    * @var DatabaseManager $dbManager
    */
    private $dbManager;

    /**
     * @var int
     */
    private $totalUniqueTags;

    /**
    * @param DatabaseManager $dbManager
    */
    public function __construct(DatabaseManager $dbManager)
    {
        $this->dbManager = $dbManager;
    }

    /**
    * @param int $storeId
    *
    * @return float
    */
    public function calculateStoreEarnings(int $storeId): float
    {
        $totalAmount = 0;
        $this->totalUniqueTags = $this->getTotalUniqueTags();

        $products = $this->getProducts($storeId);
        foreach ($products as $product) {
            $productId = $product['id'];
            $productAmount = $this->getProductAmount($storeId, $productId);
            $productAmount = $this->getProductAmountWithTagMultipliers($productId, $productAmount);

            $totalAmount += $productAmount;
        }

        return $totalAmount;
    }

    /**
    * @param int $storeId
    *
    * @return array
    */
    private function getProducts(int $storeId): array
    {
        $query = 'SELECT * FROM Product WHERE store_id = :store';

        return $this->dbManager->getData($query, ['store' => $storeId]);
    }

    /**
     * @param int $storeId
     * @param int $productId
     *
     * @return float
     */
    private function getProductAmount(int $storeId, int $productId): float
    {
        $query = '
        SELECT sum(`Product`.price) as amount
        FROM `Store`
        JOIN `Product` on `Store`.id = `Product`.store_id
        JOIN `OrderItem` on `Product`.id = `OrderItem`.product_id
        JOIN `Order` on `OrderItem`.order_id = `Order`.id
        WHERE `Store`.id = :store and `Product`.id = :product
        GROUP BY `Product`.id';

        $result = $this->dbManager->getData(
            $query,
            ['store' => $storeId, 'product' => $productId]
        );

        $resultRow = reset($result);

        return false !== $resultRow ? $resultRow['amount'] : 0;
    }

    /**
     * @param int $productId
     * @param float $productAmount
     *
     * @return float
     */
    private function getProductAmountWithTagMultipliers(int $productId, float $productAmount)
    {
        $productTags = $this->getProductTags($productId);

        $productAmount *= (1 + count($productTags) / $this->totalUniqueTags);

        foreach ($productTags as $tag) {
            if ($tag['tag_name'] === 'Christmas') {
                $productAmount *= 1.01;
            }

            if ($tag['tag_name'] === 'Free') {
                $productAmount *= 0.5;
            }
        }
        return $productAmount;
    }
    /**
    * @param int $productId
    *
    * @return array
    */
    private function getProductTags(int $productId): array
    {
        $query = '
            SELECT Tag.*
            FROM Tag
            JOIN TagConnect on Tag.id = TagConnect.tag_id
            WHERE product_id = :product';

        return $this->dbManager->getData($query, ['product' => $productId]);
    }

    /**
    * @return int
    */
    private function getTotalUniqueTags(): int
    {
        $query = 'SELECT COUNT(DISTINCT tag_name) as count FROM Tag;';

        $result = $this->dbManager->getData($query, []);

        $resultRow = reset($result);

        return false !== $resultRow ? $resultRow['count'] : 0;
    }
}
