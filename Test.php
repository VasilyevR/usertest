<?php
declare(strict_types=1);
include ('UserManager.php');
include ('User/AbstractUser.php');
include ('User/IUser.php');
include ('User/Administrator.php');
include ('User/Customer.php');
include ('User/Seller.php');

use \UserTest\User\Administrator;
use \UserTest\User\Customer;
use \UserTest\User\Seller;
use \UserTest\UserManager;

$customer = new Customer(1, 'John', '100.55', '34');
$seller = new Seller(2, 'Alex', '550.87', '781');
$administrator = new Administrator(3, 'Arnold', '{reports, sales, users}');

$userManager = new UserManager();
echo $userManager->getUserInfo($customer);
echo $userManager->getUserInfo($seller);
echo $userManager->getUserInfo($administrator);
